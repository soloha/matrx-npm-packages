function getID() {
  const id = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = crypto.getRandomValues(new Uint8Array(1))[0]%16|0, v = c == 'x' ? r : (r&0x3|0x8)
    return v.toString(16)
  })
  return id
}

function div(i, j) {
  return Math.trunc(i / j)
}

function mod(i, j) {
  return i % j
}

module.exports = {getID, div, mod}
