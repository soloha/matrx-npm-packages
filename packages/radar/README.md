# MatrX-Radar

## What is it?

MatrX-Radar is a radar visualization component.

## How to use

Refer to the example in public/index.html for parameters and data formatting

Refer to this video for information about backward breaking changes in version 1.0: https://youtu.be/FtaVwb3jTa8

The discipline is the outermost layer and groups together different practices. Each practice has a label and a description. The labels for the practices and disciplines are also autosized to fit the appropriate width. Each practice has any number of levels, and each level has a portion which represents the size of the corresponding arc.
